## 一、项目描述

- 一个基于 uniAPP、cndapp-ui框架的 " **E废钢小程序** "，通过 uniAPP 组件实现数据动态刷新渲染。
- 需要提前安装好 nodejs,下载项目后运行`npm install` 拉取依赖包。
- 项目部分区域使用了全局注册方式，增加了打包体积，在实际运用中请使用 **按需引入**。
- 拉取项目之后，建议按照自己的功能区域重命名文件，现以简单的位置进行区分。

友情链接：

1.  [uniAPP 官方文档](https://uniapp.dcloud.io/api/l)
2.  [cndapp-ui 实例](http://cndapp-ui.plusesb.com/#/)
3.  [项目 gitLib 地址（国内速度快）](https://gitlab.com/1083223299/wms-weixinTest)



## 二、主要文件介绍

| 文件                | 作用/功能  
| ------------------- | --------------------------------------------------------------------- |
| common                | 公用js组件API和样式封装                                             |
| utils                 | 公用js组件封装                                                      |
| components            | uni-app组件目录                                                      |
| pages                 | 业务tab页面文件存放的目录                                             |
| pAppointmentDelivery                | 业务预约单内部操作页面文件存放的目录                                  |
| pOperationLib                | 业务库内操作页面文件存放的目录                                        |
| static                | 存放应用引用静态资源（如图片、视频等）的目录，注意：静态资源只能存放于此 |
| main.js               | Vue初始化入口文件                                                      |
| App.vue               | 应用配置，用来配置App全局样式以及监听 应用生命周期                      |
| manifest.json         | 配置应用名称、appid、logo、版本等打包信息，详见                         |
| pages.json            | 配置页面路由、导航条、选项卡等页面类信息，详见                          |

### 请求数据
```js
// #ifdef  H5
const baseUrl = '/api'
// #endif
// #ifdef  MP-WEIXIN
const baseUrl = 'https://steelwmstest.cndmaterial.com/framework'//这里写后端地址
// #endif
// #ifdef  APP-PLUS
const baseUrl = 'https://steelwmstest.cndmaterial.com/framework';//这里写后端地址
// #endif

