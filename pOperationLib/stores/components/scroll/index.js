// CndScroll
import CndScroll from './mescroll-uni.vue';
CndScroll.install = Vue => Vue.component(CndScroll.name, CndScroll);
export default CndScroll;