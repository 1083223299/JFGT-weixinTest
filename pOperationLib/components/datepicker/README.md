## DatePicker 日期选择器

### 说明

从底部弹出的日期选择器，提供可供修改的时间层级

### 引入

```javascript
<script>
    import cndDatepicker from '../datepicker/cnd-datepicker.vue'
        
    components:{
        cndDatepicker
    },
</script>
```

### 代码演示
```html
	<cnd-datepicker :value="datej" :level="3" @change="bindChange">{{timeRes ? timeRes : '请选择时间'}}</cnd-datepicker>
	// 新版
	<cnd-datepicker :level="3" start="1980" end="2200" placeholder="请选择日期时间" defaultValue="2020-09-08"></cnd-datepicker>
```
```javascript
	data() {
		return {
			timeRes: '',
			datej: new Date().format('yyyy-MM-dd')
		}
	},
```

####  props

| 属性 | 说明 | 类型 | 默认值 | 备注 |
| ---- | ----- | ------ | ------ | ------ |
| value | 绑定时间 | String | `new Date().format('yyyy-MM-dd hh:mm')` | - |
| level | 展示层级 | Number | `1` | 从年开始逐层递增 | - |
| disabled | 是否禁用 | Boolean | `false` | - |


####  events
| 事件 | 说明 | 回调参数 |
| ---- | ---- | ---- |
| change | 选中操作 | 选中日期的字符串格式 |

####  新版props

| 属性 | 说明 | 类型 | 默认值 | 备注 |
| ---- | ----- | ------ | ------ | ------ |
| level | 时间选择器层级 | String || Number | `1` | - |
| start | 有效日期时间范围的开始 | String | `1970/01-01 00:00` | `格式为 "YYYY-MM-DD hh:mm"` |
| end | 有效日期时间范围的结束 | String | `n2300-01-01 00:00` | `格式为 "YYYY-MM-DD hh:mm"` |
| placeholder | 占位符 | String | `请选择日期时间` | - |
| disabled | 是否禁用 | Boolean | `false` | - |
| defaultValue | 默认值 | `''` | `false` | `格式为 "YYYY-MM-DD hh:mm` |


####  新版events
| 事件 | 说明 | 回调参数 |
| ---- | ---- | ---- |
| change | 选中操作 | 从年份到秒到各级数据以及各种常用格式,具体看返回值 |
