// CndNavbar
import CndNavbar from './cnd-navbar.vue';
CndNavbar.install = Vue => Vue.component(CndNavbar.name, CndNavbar);
export default CndNavbar;