## checkbox复选框
### 说明

复选框。复选框是一种可同时选中多项的基础控件

### 引入

```javascript
<script>
    import CndCheckBoxGroup from "../checkbox-new/cnd-checkbox-group.vue"  
    import CndCheckBox from "../checkbox-new/cnd-checkbox.vue"
    components:{
		CndCheckBoxGroup,
		CndCheckBox
    },
</script>
```

### 代码演示
```html
	//单独使用
	<cnd-checkbox v-model="checked" @change="bindCheckboxSingleChange">选项一</cnd-checkbox>
	//组合使用
	<cnd-checkbox-group v-model="multiple" @change="bindCheckboxGroupChange">
		<block v-for="(item, index) in testOptions" :key="index">
			<view class="mgb24">
				<cnd-checkbox :label="item.sValue">{{item.sName}}</cnd-checkbox>
			</view>
		</block>
	</cnd-checkbox-group>
```
```javascript
	data() {
		return {
			checked: true,
			multiple: ['option1', 'option3'],
			testOptions: [{
				sName: 'aaa',
				sValue: 'option1'
			}, {
				sName: 'bbb',
				sValue: 'option2'
			},{
				sName: 'ccc',
				sValue: 'option3'
			},{
				sName: 'ddd',
				sValue: 'option4'
			},{
				sName: 'eee',
				sValue: 'option5'
			}]
		}
	}
```

#### CndCheckbox props

| 属性 | 说明 | 类型 | 默认值 | 备注 |
|----|-----|------|------ |------|
| value / v-model | 绑定值 | String / Number / Boolean | `false` | - |
| label | 选中状态的值（只有在cnd-checkbox-group或者绑定对象类型为array时有效）| String / Number / Boolean | - | - |
| disabled | 是否禁用 | Boolean | `false` | - |
| checked | 当前是否勾选 | Boolean | `false` | - |

#### CndCheckbox Events
| 事件名 | 说明 | 回调参数 |
| ------ | --- | ----- |
| change | 绑定值变化时触发 | 更新后的值 |

#### CndCheckboxGroup props

| 属性 | 说明 | 类型 | 默认值 | 备注 |
|----|-----|------|------ |------|
| value / v-model | 绑定值 | Array | [] | - |

#### CndCheckboxGroup events

| 事件名 | 说明 | 回调参数 |
| ------ | --- | ----- |
| change | 绑定值变化时触发 | 更新后的值 |

