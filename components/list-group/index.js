// CndListGroup
import CndListGroup from './list-group.vue';
CndListGroup.install = Vue => Vue.component(CndListGroup.name, CndListGroup);
export default CndListGroup;