const getCurVersion = () => {
	return new Promise((resolve, reject) => {
		plus.runtime.getProperty(plus.runtime.appid, (widgetInfo) => {
			resolve(widgetInfo.version)
		})
	})
}
export {
	getCurVersion
}
