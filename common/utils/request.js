import { baseUrl, getReqTime } from '@/common/utils/index.js'

function Request(url, data) {
	// console.log('url', url)
	// console.log('请求参数', data)
	let requestURL = baseUrl + '/' +  url; //测试库
	let ReqTime = getReqTime();
	return new Promise((resolve, reject) => {
		const static_header = {
			"code": 0,
			"message": {
				"title": "",
				"detail": ""
			}
		}
		uni.request({
			url: `${requestURL}?ajaxRequest=true&t=${ReqTime}`,
				data: {
					header: static_header,
					body: data
				},
				header: {
					'Accept-Language':'zh-CN,zh;q=0.9',  
					'Content-Type': 'application/json',
					// 'Cookie': 'JSESSIONID=' + uni.getStorageSync('Cookie')
				},
				method: 'POST',
				withCredentials: true,
				success: (res) => {
					// console.log('成功结果：', res)
					if(res.statusCode === 403) {
						// let Cookie = uni.getStorageSync('Cookie')
						let j_phone = uni.getStorageSync('j_phone')
						let sms_code = uni.getStorageSync('sms_code')
						let j_password = '666999'
						console.log('打印手机号和验证码',j_phone,sms_code)
						// uni.$emit('setInfo')
						if(j_phone == ''){
							uni.reLaunch({
								url: '/pages/login/login'
							})
							return
						}
						uni.showLoading({
							title: '...',
							mask:true
						})
						uni.request({
							url: `${baseUrl}/j_spring_security_sms_code?j_phone=${j_phone}&j_sms_code=${j_password}&useRestful=true`,
							method: "POST",
							header: {
								"content-type": "application/x-www-form-urlencoded",
							},
							success: res => {
								uni.hideLoading()
								// uni.setStorageSync('j_username', j_username)
								uni.setStorageSync("Cookie", res.data.id)
								// #ifdef H5
								document.cookie = 'JSESSIONID=' + res.data.id
								// #endif
								// location.reload()
								uni.$emit('setInfo')
								if (res.data.code == '1') {
									uni.showModal({
										title: '提示',
										content: '登录异常，请检查后重登！',
										success: function (res) {
											if (res.confirm) {
												uni.removeStorageSync('j_phone')
												uni.removeStorageSync('sms_code')
												uni.reLaunch({
													url: '/pages/login/login'
												})
											} else if (res.cancel) {
												uni.removeStorageSync('j_phone')
												uni.removeStorageSync('sms_code')
												uni.reLaunch({
													url: '/pages/login/login'
												})
											}
										}
									})
								}
							},
							fail: err => {
								console.log('err', err)
								uni.showToast({
									title: '未知异常！',
									icon: 'none',
									mask: true
								})
							}
						})
						// uni.showModal({
						// 	title: '提示',
						// 	content: '登录信息过期',
						// 	showCancel: false,
						// 	confirmText: '重新登录',
						// 	success: (res) => {
						// 		if(res.confirm) {
						// 			uni.reLaunch({
						// 				url: '/pages/login/login'
						// 			})
						// 		}
						// 	}
						// })
					} else {
						resolve(res.data.body)
					}
					if(res.data.header.code != 0) { 
						let strCode = res.data.header.message.title
						if(strCode.length > 20){
							uni.$emit('headerCode',strCode)
						}else{
							uni.showToast({
								title: `${strCode}`,
								icon: 'none'
							})
						}
					}
				},
			fail: err => {
				// // console.log('err', JSON.stringify(err))
				// console.log('err res', err)
				reject(err)
			}
		})
	}).catch(err => {
		let title = err.errMsg;
		uni.showToast({
			title: title,
			icon: 'none',
			mask: true,
			duration: 2000
		});
	})
}


export default function(url, data) {
	return Request(url, data)
}
