import { esbBaseUrl, esbBaseCode, esbBaseAppKey } from '@/common/utils/index.js'
import { getUUID, getReqTime } from './index.js'

function Request(comm_r, data) {
	// console.log('请求参数', data)
	let requestURL = esbBaseUrl; //测试库
	let ReqSysCode = esbBaseCode;
	let ReqTime = getReqTime();
	let uuid = getUUID();
	let Appkey = esbBaseAppKey;
	let SessionId = uni.getStorageSync('sessionID') || "";
	let usercode = uni.getStorageSync("userCode") || "";
	return new Promise((resolve, reject) => {
		uni.request({
			url: requestURL,
			method: "POST",
			header: {
				"content-type": "application/x-www-form-urlencoded"
			},
			data: JSON.stringify({
				"Service": {
					"Body": {
						"Request": data
					},
					"Header": {
						"Request": {
							"Bcode": '',
							"ReqSysCode": ReqSysCode,
							"ReqTime": ReqTime,
							"ReqType": "0",
							"Security": {
								"AppKey": esbBaseAppKey,
								"SessionId": SessionId
							},
							"SerialNumber": ReqSysCode + uuid,
							"ServiceCode": comm_r,
							"Ucode": usercode
						}
					}
				}
			}),
			success: res => {
				// uni.hideLoading()
				let header = res.data.Service.Header.Response;
				if (header && header.ReturnCode == "000000002") {
					//过期
					uni.showToast({
						title: '登录超时,请重新登录',
						icon: 'none',
						duration: 500
					});
					uni.clearStorageSync();
					uni.reLaunch({
						url: '/pages/error/index'
					})
				}
				if(res.data.Service.Header.Response.ReturnCode==='000000001'){
					uni.showToast({title:res.data.Service.Header.Response.ReturnMessage,icon:'none',duration:2000})
				}
				resolve(res.data.Service.Body.Response)
			},
			fail: err => {
				uni.hideLoading()
				// // console.log('err', JSON.stringify(err))
				console.log(err)
				reject(err)
			}
		})
	}).catch(err => {
		uni.hideLoading()
		let title = err.errMsg;
		uni.showToast({
			title: title,
			icon: 'none',
			mask: true,
			duration: 2000
		});
	})
}


export default function(comm_r, data, bcode, isReturn) {
	return Request(comm_r, data, bcode, isReturn)
}
