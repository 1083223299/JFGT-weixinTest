import axios from 'axios'
import { baseUrl } from '@/common/utils/index.js'
import Qs from 'qs'
const service = axios.create({
    baseURL: 'http://119.3.14.155:8080', // url = base url + request url
    withCredentials: true, // send cookies when cross-domain requests
    // timeout: 5000, // request timeout
    crossDomain: true
})
export default function post(url, data = {}, timeout = 15) {
  let reqData = JSON.stringify(data)
  let contentType = 'application/json'
  try {
    let config = {
      url: baseUrl + url,
      method: 'post',
      headers: { 'Content-Type': contentType, Cookie: 'A2A5428FC684563327D92FA7C17BC422'},
      withCredentials: true,
      data: reqData,
      timeout: timeout * 1000,
	  Date: new Date()
    }
	// config['headers']['Authorization'] = '434929966AB109F995821E0679B52E98'
    return service(config)
  } 
  catch (e) {
    return { code: -1, msg: '网络异常' }
  }
}