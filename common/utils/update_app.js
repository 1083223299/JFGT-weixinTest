import { getVersion } from '@/common/api/api.js'
import { projectKey, version_need_update } from '@/common/utils/index.js'
// openType 为1时为首次开启应用即 onLaunch
export function update_app(openType = "onShow") {
	// #ifdef APP-PLUS
	// uni.setEnableDebug({
	//     enableDebug: true
	// })
	console.log('plus.runtime.version', plus.runtime.version)
	var isBig = false
	
	// plus.runtime.getProperty(plus.runtime.appid, (widgetInfo) => {
		// console.log('widgetInfo', widgetInfo)
		let type = plus.os.name === 'Android' ? 'apk' : 'plist'
		let versionReq = {
			"data": [
				{
					"appID":"0",
					"projectKey": projectKey,
					"type":type,
					"version": plus.runtime.version
				}
			]
		}
		getVersion('S10_VERSION_R_00002', versionReq).then(res => {
			console.log('res', res)
			console.log('版本req', versionReq)
			console.log("大版本更新启动…", openType)
			let versionData = res[0]
			console.log('版本', versionData)
			// let old_version = widgetInfo.version
			let old_version = plus.runtime.version
			let new_version = versionData.version
			console.log('版本比较', old_version, versionData.version, version_need_update(old_version, new_version))
			// 判断是否大版本更新
			if(version_need_update(old_version, new_version)) {
				isBig = true
				if(versionData.description.includes('强制更新')){
					// 强制更新
					console.log('大版本更新中…,强制更新')
					update_big(versionData.url)
				} else {
					console.log("大版本更新中…,等待用户确认" + versionData.url)
					uni.showModal({
						//提醒用户更新
						title: '更新提示',
						content: `当前版本${old_version}, 有新版本${new_version}, 是否更新?`,
						success: res => {
							if(res.confirm) {
								console.log("大版本更新,用户点击确认")
								update_big(versionData.url)
							} else if(res.cancel) {
								console.log('大版本更新,用户点击取消')
							}
						},
						fail: () => {
							console.log("大版本更新,系统错误")
							return
						}
					})
				}
			} else {
				// 非大版本更新,判断是否需要小版本小更新
				update_app_s(openType)
			} 
		})
	// })   
	// #endif
}
function get_version_request(reqForm) {
	return getVersion('S10_VERSION_R_00002', reqForm, false).then(async (res) => res)
}
export function force_update() {
	// #ifdef APP-PLUS
	plus.runtime.getProperty(plus.runtime.appid, function(widgetInfo) {
		var wgtWaiting = null;
		let type = "plist";
		
		if (plus.os.name == 'Android') {
			type = "apk";
		}
		
		let val ={
			"data":[
			{
				"appID":"0",
				"projectKey": projectKey,
				"type":type,
				"version": widgetInfo.version
			}
		]};
	});
	// #endif
}

function update_app_s(openType = "onShow") {
	// #ifdef APP-PLUS
	plus.runtime.getProperty(plus.runtime.appid, function(widgetInfo) {
		var wgtWaiting = null;
		
		let type = "plist";
		let appID = "WI00001";
		
		if (plus.os.name == 'Android') {
			type = "apk";
			appID = "WI00001";
		}
		
		//小版本更新
		let val_s ={
			"data":[
			{
				"appID": appID,
				"projectKey": projectKey,
				"type":"html",
				"version": widgetInfo.version
			}
		]};
		getVersion('S10_VERSION_R_00002',val_s, false).then(res => {
			console.log("小版本更新启动…",openType)
			console.log('小版本req', val_s)
			console.log('小版本res', res)
			let data = res
			console.log('小版本resdata', data);
			if(data[0].isLatest === 1){
				return;
			}
			let new_version = data[0].version;
			let old_version = widgetInfo.version;
			var news = new_version.split('.');
			var olds = old_version.split('.');
			console.log(old_version, new_version)
			if(version_need_update(old_version, new_version)){
				update_small(data[0].url)
				// if(data.description.includes('强制更新')){
				// 	// 强制更新
				// 	console.log('小版本更新中…,强制更新')
				// 	update_small(data.url)
				// } else {
				// 	console.log("小版本更新中…,等待用户确认" + data.url);
				// 	update_small(data.url)
				// 	uni.showModal({
				// 		//提醒用户更新
				// 		title: '更新提示',
				// 		content: `当前版本${old_version}, 有新版本${new_version}, 是否更新?`,
				// 		success: res => {
				// 			if (res.confirm) {
				// 				console.log("小版本更新,用户点击确认");
				// 				update_small(data.url)
				// 			} else if (res.cancel) {
				// 				console.log('小版本更新,用户点击取消');
				// 			}
				// 		},
				// 		fail:function(){
				// 			console.log("小版本更新,系统错误")
				// 			return
				// 		}
				// 	});
				// }
			}
		})
	});
	// #endif
}

function update_big(url){
	if (plus.os.name == 'Android') {
		var wgtWaiting = plus.nativeUI.showWaiting('开始下载');
		var dtask = plus.downloader.createDownload(url, {}, function(d, status) {
			// 下载完成
			// wgtWaiting.setTitle('开始安装');
			// uni.showToast({
			// 	title: `${JSON.stringify(d)}, ${JSON.stringify(status)}`,
			// 	icon: 'none'
			// })
			if (status == 200) {
				plus.runtime.install(
					d.filename,
					{},
					function(wgtinfo) {
						wgtWaiting.close();
					},
					function(error) {
						wgtWaiting.close();
						uni.showToast({  
							title: '应用更新失败!',  
							mask: false,  
							duration: 1500  ,
						});  
					}
				);
			} else {
				wgtWaiting.close();
				uni.showToast({  
					title: '应用更新失败!',  
					mask: false,  
					duration: 1500  ,
				});  
			}
		});
		dtask.addEventListener('statechanged', function(download, status) {
			switch (download.state) {
				case 2:
					wgtWaiting.setTitle('已连接到服务器');
					break;
				case 3:
					var percent = (download.downloadedSize / download.totalSize) * 100;
					wgtWaiting.setTitle('已下载 ' + parseInt(percent) + '%');
					break;
				case 4:
					wgtWaiting.setTitle('下载完成');
					break;
			}
		});
		dtask.start();
	} else {
		plus.runtime.openURL(url);
	}
}


function update_small(url){
	let wgturl = url.replace(".zip",".wgt");
	console.log(url, wgturl)
	uni.downloadFile({
		url: wgturl,
		success: downloadResult => {
			console.log('downLoadResult', downloadResult)
			if (downloadResult.statusCode === 200) {
				uni.showLoading()
				plus.runtime.install(
					downloadResult.tempFilePath,
					{
						force: false
					},
					function() {
						console.log('install success...');
						uni.hideLoading()
						uni.showToast({
							title: '更新完成',
							icon: 'success',
							duration: 1400,
						});
						setTimeout(()=>{
							console.log("重启!")
							plus.runtime.restart();
						},1500)
					},
					function(e) {
						console.log(JSON.stringify(e));
						console.error('install fail...');
						uni.hideLoading()
						uni.showToast({
							title: e.message,
							icon: 'none'
						})
					}
				);
			}
		},
		fail: () => {
			uni.showToast({
				title: '应用更新失败!',  
				mask: false,  
				duration: 1500,
			});  
		}
	});
}