import Vue from 'vue'
import App from './App'
import storePop from './store/store'
import mpopup from '@/components/pop-content/popup.vue'//引入
// import CndApp from 'cndapp-ui'
// import 'cndapp-ui/common/style/iconfont/iconfont.css'
import Request from '@/common/utils/request.js'
import share from '@/common/share.js'
// create an axios instance
// 防止重复点击
import global from '@/common/utils/global.js'
import 'weapp-cookie/dist/weapp-cookie'
import util from '@/utils/utils.js'
import store from './store/index'
import uniPopup from '@/components/uni-popup/uni-popup.vue'
import uniPopupDialog from '@/components/uni-popup-dialog/uni-popup-dialog.vue'
import xLoading from '@/components/loading/index.vue'

// import Post from '@/common/utils/axios.js'
// Vue.prototype.$post = Post
function showPopup(list){
	storePop.commit("commit_mpopup",list)
}
Vue.component('uniPopup', uniPopup)
Vue.component('uniPopupDialog', uniPopupDialog)
Vue.component('xLoading', xLoading)
Vue.prototype.$noMultipleClicks = global.noMultipleClicks;
Vue.prototype.$http = Request
Vue.prototype.$storePop = storePop
Vue.component('my-popup',mpopup);
//注册全局
Vue.prototype.$store = store;
Vue.prototype.$showPopup = showPopup;
Vue.prototype.$util = util
Vue.config.productionTip = false
Vue.mixin(share)
App.mpType = 'app'
// Vue.use(CndApp)
const app = new Vue({
    ...App,
    share
})
if (uni.getSystemInfoSync().platform !== "devtools") { //禁止console.log打印
	// console.log = () => {}
}
app.$mount()
