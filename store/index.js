import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
const store = new Vuex.Store({
    state: {
		page: 2,
		// 登录状态
		uerInfo: {},  
        hasLogin: false
	},
    mutations: {
		SET_PAGE(state, value) {
			state.page = value || 1
		},
		// 存储登录状态
		logins(state, provider) {//改变登录状态  
            state.hasLogin = true  
            state.uerInfo.toSID = provider.toSID  
            // state.uerInfo.userName = provider.user_name 
            uni.setStorage({//将用户信息保存在本地  
                key: 'uerInfo',  
                data: provider  
            })  
        },  
        logout(state) {//退出登录  
            state.hasLogin = false  
            state.uerInfo = {}  
            uni.removeStorage({  
                key: 'uerInfo'  
            })  
        } 
	},
    actions: {
		toSetPage(context, val) {
			context.commit('SET_PAGE', val)
		}
	},
	getters: {
		page: state => state.page
	}
})
export default store